/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistempenjualan;

import java.awt.Desktop;
import java.net.URI;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import koneksi.koneksi;

/**
 *
 * @author U
 */
public class Transaksi2 extends javax.swing.JDialog {

    /**
     * Creates new form Transaksi2
     */
     public Statement st;
    public ResultSet rs;
    public DefaultTableModel tabmodel;
    public long bayarr;
    public long totall;
    public long kembaliann;
    
    Connection cn = koneksi.getkoneksi();
    
    public Transaksi2(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        autokode();
        tampilBarang();
        getTotal();
    }
    
    public void tampilBarang(){
        try{
            Object[] baris = {"Barang", "Jumlah", "Total"};
            tabmodel = new DefaultTableModel(null, baris);
            st = cn.createStatement();
            rs = st.executeQuery("select * from tbl_bantu inner join tbl_barang ON tbl_bantu.kd_barang = tbl_barang.kd_barang where tbl_bantu.kd_transaksi='"+txtKode.getText()+"'");
            while (rs.next()){
                String tgl = rs.getString("nama");
                String barang = rs.getString("qty");
                String ttl = rs.getString("total");
                String[] row = {tgl, barang, ttl};
                tabmodel.addRow(row);
            }
            jTable1.setModel(tabmodel);
         
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    public void autokode(){
        try{
            String sql;
            st = cn.createStatement();
            sql = "select max(kd_transaksi) as kode from tbl_transaksi";
            rs =  st.executeQuery(sql);
            if(rs.next()){
                
                String id = rs.getString("kode").substring(1);
                String kode = ""+(Integer.parseInt(id)+1);
                if (kode.length()==1){
                    txtKode.setText("T00"+kode);
                }else if(kode.length()==2){
                    txtKode.setText("T0"+kode);
                }else{
                    txtKode.setText("T"+kode);
                }
            }else
                txtKode.setText("T001");
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    private String getKodeBantu(){
        try{
            String sql;
            st = cn.createStatement();
            sql = "select max(kd_transaksi) as kode from tbl_transaksi";
            rs =  st.executeQuery(sql);
            if(rs.next())
                return rs.getString("kode");
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return "";
    }
    
    private void getTotal(){
        try{
            String sql;
            st = cn.createStatement();
            sql = "select SUM(total) as total from tbl_bantu where kd_transaksi='"+txtKode.getText()+"'";
            rs =  st.executeQuery(sql);
            if(rs.next())
                txtTotal.setText(rs.getString("total"));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    private void selesai(){
        try{
            st = cn.createStatement();
            String sql;
            sql = "insert into tbl_transaksi values('"+txtKode.getText()+"', '"+txtNama.getText()+"', '"+txtTelepon2.getText()+"', '"+txtTotal.getText()+"', '"+txtBayar.getText()+"', '"+txtKembalian.getText()+"')";
            st.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, "Selesai menambah data");
            reset();
            tampilBarang();
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    private void bayar(){
        bayarr = Integer.parseInt(txtBayar.getText());
        totall = Integer.parseInt(txtTotal.getText());
        
        if(bayarr < totall){
            JOptionPane.showMessageDialog(null, "Uang Kurang");
        }else{
            kembaliann = bayarr - totall;
            txtKembalian.setEnabled(false);
            txtKembalian.setText(Long.toString(kembaliann));
            
        }
    }
    
    private void reset(){
        txtKode.setText(null);
        txtNama.setText(null);
        txtTelepon2.setText(null);
        txtTotal.setText(null);
        txtBayar.setText(null);
        txtKembalian.setText(null);
        
        autokode();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        txtTotal = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        txtKode = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        txtNama = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtTelepon2 = new javax.swing.JTextField();
        txtBayar = new javax.swing.JTextField();
        txtKembalian = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/tumblr_static_galaxy_desktop_wallpapers_1.jpg"))); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTable1.setFont(new java.awt.Font("Tekton Pro", 0, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 52, 383, 236));

        txtTotal.setEnabled(false);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        getContentPane().add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(481, 90, 186, -1));

        jLabel4.setFont(new java.awt.Font("Tekton Pro", 0, 14)); // NOI18N
        jLabel4.setText("Total");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(411, 93, -1, -1));

        jButton1.setFont(new java.awt.Font("Tekton Pro", 0, 14)); // NOI18N
        jButton1.setText("Tambah Belanja");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, -1, -1));

        jButton2.setFont(new java.awt.Font("Tekton Pro", 0, 14)); // NOI18N
        jButton2.setText("Selesai");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 270, -1, -1));

        txtKode.setEnabled(false);
        getContentPane().add(txtKode, new org.netbeans.lib.awtextra.AbsoluteConstraints(481, 52, 186, -1));

        jLabel7.setFont(new java.awt.Font("Tekton Pro", 0, 14)); // NOI18N
        jLabel7.setText("Kode");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(411, 55, -1, -1));

        jButton3.setFont(new java.awt.Font("Tekton Pro", 0, 14)); // NOI18N
        jButton3.setText("Refresh");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(277, 11, 116, -1));

        txtNama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNamaKeyTyped(evt);
            }
        });
        getContentPane().add(txtNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(481, 128, 186, -1));

        jLabel8.setFont(new java.awt.Font("Tekton Pro", 0, 14)); // NOI18N
        jLabel8.setText("Kembalian");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 230, -1, -1));

        jLabel9.setFont(new java.awt.Font("Tekton Pro", 0, 14)); // NOI18N
        jLabel9.setText("Telepon");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(411, 169, -1, -1));

        jLabel10.setFont(new java.awt.Font("Tekton Pro", 0, 14)); // NOI18N
        jLabel10.setText("Nama");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(411, 131, -1, -1));

        jLabel11.setFont(new java.awt.Font("Tekton Pro", 0, 14)); // NOI18N
        jLabel11.setText("Nama");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(411, 131, -1, -1));

        jLabel12.setFont(new java.awt.Font("Tekton Pro", 0, 14)); // NOI18N
        jLabel12.setText("Bayar");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 200, -1, -1));

        txtTelepon2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelepon2KeyTyped(evt);
            }
        });
        getContentPane().add(txtTelepon2, new org.netbeans.lib.awtextra.AbsoluteConstraints(481, 166, 186, -1));

        txtBayar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBayarActionPerformed(evt);
            }
        });
        getContentPane().add(txtBayar, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 200, 190, -1));
        getContentPane().add(txtKembalian, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 230, 190, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/hell.jpg"))); // NOI18N
        jLabel2.setText("jLabel2");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        new Transaksi(null, true).setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        
       tampilBarang();
       getTotal();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if(txtKode.getText().trim().equals("")||txtNama.getText().trim().equals("")||txtTelepon2.getText().trim().equals("")||txtBayar.getText().trim().equals("")){
        JOptionPane.showMessageDialog(null, "Data Belum Diisi dengan Lengkap");
        }else{
        
            if(Desktop.isDesktopSupported()){
            try{
                Desktop.getDesktop().browse(new URI("http://localhost:81/laporanjava/struk.php?id=" + txtKode.getText()));
            } catch(Exception e){
                e.printStackTrace();
            }
        }
            selesai();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtNamaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNamaKeyTyped
        if(Character.isDigit(evt.getKeyChar())){
            evt.consume();
        }
    }//GEN-LAST:event_txtNamaKeyTyped

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void txtTelepon2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelepon2KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTelepon2KeyTyped

    private void txtBayarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBayarActionPerformed
        // TODO add your handling code here:
        bayar();
    }//GEN-LAST:event_txtBayarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Transaksi2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Transaksi2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Transaksi2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Transaksi2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Transaksi2 dialog = new Transaksi2(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtBayar;
    private javax.swing.JTextField txtKembalian;
    private javax.swing.JTextField txtKode;
    private javax.swing.JTextField txtNama;
    private javax.swing.JTextField txtTelepon2;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
}
